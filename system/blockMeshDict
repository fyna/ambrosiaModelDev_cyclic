/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  4.x                                   |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      blockMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
#include "../meshParas"
convertToMeters 1;
pi 	3.14159265359;

//-------------------------geometry-------------------------------
lx		#calc "0.5*$sideLength";
ly		$lx;
lz		$height;
r0		$spacerRad;
r1		#calc "1.2*$r0";
phi		25;	// [degree], !<30
phiRad		#calc "$pi*$phi/180.0";	// [rad]
alpha		#calc "acos( (0.5*$lz-$r0)/$r1 )";
beta		#calc "acos( ($r0-0.5*$lz)/$r1 )";


//-------------------------resolution-------------------------------
rho_x		30.0;	// [cells/mm]
rho_z		30.0;	// [cells/mm]


// spacer walls (thickness defined through r1-r0):	
// nbBlSpc		#calc "int(1000.0*$rho_z*3.0/9.0*$lz)";	// nb layers;	// nb of layers


nbBlSpc		15;
nz_0		$nbBlSpc;
// nz_0		#calc "int(1000.0*$rho_z*3.0/9.0*$lz)";	// nb layers

alphaX1		#codeStream 
		{
		    codeInclude
		    #{
			#include "fvCFD.H"
		    #};
		    codeOptions
		    #{
			-I$(LIB_SRC)/finiteVolume/lnInclude \
			-I$(LIB_SRC)/meshTools/lnInclude
		    #};
		    codeLibs
		    #{
			-lmeshTools \
			-lfiniteVolume
		    #};
		    code
		    #{
			float a = ($r1 - $r0)*$rho_z*1000.0;		//ratio delta_bl / delta_b
			float r = 0.001;		
			int N = $nbBlSpc;
			float nom, denom;
			for(int i=0; i<100; i++){
			    nom 	= pow(r,N+1) -(a+1.0)*r +a;
			    denom 	= (N+1)*pow(r,N) -a-1.0;
			    r = r - nom/denom ;
			}
			os << pow(1.0/r,(N-1));
		    #};
		};

alphaM1		$alphaX1;	// ratio biggest/smallest

// bulk:
nx_0		#calc "int(1000.0*$rho_x*($lx-$r0))";
nx_1		#calc "int(1000.0*$rho_x*0.8*$r0)";
nx_2		#calc "int(1000.0*$rho_x*$r0)";

nz_1		#calc "int(1000.0*$rho_z*2.0/9.0*$lz)";    	// 1/4 bulck

// dependend:
alphaY1		#calc "1.0/$alphaX1";
alphaM2		#calc "1.0/$alphaM1";

ny_0		$nx_0;		
ny_1		$nx_1;		
ny_2		$nx_2;
		
nz_2		$nz_1;
nz_3		$nz_2;
nz_4		$nz_1;
nz_5		$nz_0;

//-------------------------coordinates-------------------------------

x0	0;
x1	#calc "$lx-$r1";
x2	#calc "$lx-$r0";
x3	#calc "$lx-$r1*sin(0.75*$pi)";
x4	#calc "$lx-$r0*sin(0.75*$pi)";
x5	#calc "$lx-$r0*sin($pi-$phiRad)";
x6	$lx;
x7	#calc "$lx-$r1*sin($alpha)";
x8	#calc "$lx-$r0*sin($alpha)";

y0	0;
y1	#calc "$ly-$r1";
y2	#calc "$ly-$r0";
y3	#calc "$ly-$r1*sin(0.75*$pi)";
y4	#calc "$ly-$r0*sin(0.75*$pi)";
y5	#calc "$ly-$r0*sin($pi-$phiRad)";
y6	$ly;
y7 	#calc "$ly-$r1*sin($beta)";
y8 	#calc "$ly-$r0*sin($beta)";

z0	0;
z1	#calc "0.5*($r0+$r0*cos($pi-$phiRad))";
z2	#calc "$r0+$r0*cos($pi-$phiRad)";
z3	#calc "$r0+$r1*cos(0.75*$pi)";
z4	#calc "$r0+$r0*cos(0.75*$pi)";
//z5	$r0; 	//redefined later
z6	#calc "$r0+$r0*cos($alpha)";	
z7	#calc "0.5*$lz";	
z8	#calc "2.0*$r0";
z9	#calc "$r0+$r1";
//z10	#calc "$lz-$r0";	//redefined later
z11	#calc "$lz-$r0+$r1*cos(0.25*$pi)";
z12	$lz;
z13	#calc "0.5*(2.0*$lz-$r0+$r0*cos($phiRad))";
z14	#calc "$lz-$r0+$r0*cos($phiRad)";
z15	#calc "$lz-$r0+$r0*cos(0.25*$pi)";
z16	#calc "$lz-$r0+$r0*cos($beta)";
z17	#calc "$lz-$r0+$r1*cos(0.75*$pi)";
z18	#calc "$lz-2.0*$r0";
z19	#calc "$lz-$r0-$r1";
z20	#calc "0.5*($z9+$z11)";
z21	#calc "0.5*($z19+$r0+$r1*cos(0.75*$pi))";
z10	#calc "0.5*($z20+$lz-$r0)";
z5	#calc "0.5*($z21+$r0)";

// angle angle dependend coordinates 
phiY81	#calc "acos(($z20-$lz+$r0)/$r0)";	
phiY66	#calc "acos(($z20-$lz+$r0)/$r1)";	
y81	#calc "$ly-$r0*sin($phiY81)";
y66	#calc "$ly-$r1*sin($phiY66)";

phiY82	#calc "acos(($z9-$lz+$r0)/$r0)";	
phiY63	#calc "acos(($z9-$lz+$r0)/$r1)";	
y82	#calc "$ly-$r0*sin($phiY82)";
y63	#calc "$ly-$r1*sin($phiY63)";

phiY84	#calc "acos(($z8-$lz+$r0)/$r0)";	
phiY62	#calc "acos(($z8-$lz+$r0)/$r1)";	
y84	#calc "$ly-$r0*sin($phiY84)";
y62	#calc "$ly-$r1*sin($phiY62)";

phiY85	#calc "acos(($z7-$lz+$r0)/$r0)";	
phiY53	#calc "acos(($z7-$lz+$r0)/$r1)";	
y85	#calc "$ly-$r0*sin($phiY85)";
y53	#calc "$ly-$r1*sin($phiY53)";

phiX85	#calc "acos(($z7-$r0)/$r0)";
phiX53	#calc "acos(($z7-$r0)/$r1)";
x85	#calc "$lx-$r0*sin($phiX85)";
x53	#calc "$lx-$r1*sin($phiX53)";

phiX83	#calc "acos(($z19-$r0)/$r0)";
phiX64	#calc "acos(($z19-$r0)/$r1)";
x83	#calc "$lx-$r0*sin($phiX83)";
x64	#calc "$lx-$r1*sin($phiX64)";

phiX86	#calc "acos(($z18-$r0)/$r0)";
phiX65	#calc "acos(($z18-$r0)/$r1)";
x86	#calc "$lx-$r0*sin($phiX86)";
x65	#calc "$lx-$r1*sin($phiX65)";

phiX80	#calc "acos(($z21-$r0)/$r0)";
phiX67	#calc "acos(($z21-$r0)/$r1)";
x80	#calc "$lx-$r0*sin($phiX80)";
x67	#calc "$lx-$r1*sin($phiX67)";


elpA	#calc "$y82-$y62";
elpB	#calc "$z9-$z8";
phiS	#calc "$pi/4.0";

rPhi	#calc "$elpA*$elpB/sqrt( pow($elpB*cos($phiS),2) +pow($elpA*sin($phiS),2))";
y84_2	#calc "$y62+$rPhi*cos($phiS)";
z84_2	#calc "$z9-$rPhi*sin($phiS)";

x85_2	#calc "$x7+0.6*($x8-$x7)";
y85_2	#calc "$y7+0.7*($y8-$y7)";
z85_2	#calc "$z6+0.5*($z16-$z6)";

x86_2	#calc "$x64+0.7*($x83-$x64)";
z86_2	#calc "$z19+0.7*($z18-$z19)";


// all vertices in plane Y=y0 first, then Y=y1 vertices...
// in a plane z coordinates are defined before x coordinates. 
vertices
(
// y0 plane
    ($x0 $y0 $z0)	//0
    ($x0 $y0 $z3)
    ($x0 $y0 $z5)
    ($x0 $y0 $z7)
    ($x0 $y0 $z10)
    ($x0 $y0 $z11)	//5
    ($x0 $y0 $z12)
    ($x1 $y0 $z5)
    ($x2 $y0 $z5)
    ($x3 $y0 $z0)
    ($x3 $y0 $z3)	//10
    ($x7 $y0 $z7)
    ($x7 $y0 $z10)
    ($x7 $y0 $z11)
    ($x7 $y0 $z12)
    ($x4 $y0 $z4)	//15
    ($x8 $y0 $z6)
    ($x5 $y0 $z0)
    ($x5 $y0 $z1)
    ($x5 $y0 $z2)
    ($x6 $y0 $z8)	//20
    ($x6 $y0 $z9)
    ($x6 $y0 $z20)
    ($x6 $y0 $z11)
    ($x6 $y0 $z12)
// x0 plane
    ($x0 $y1 $z10)	//25
    ($x0 $y2 $z10)
    ($x0 $y7 $z0)
    ($x0 $y7 $z3)
    ($x0 $y7 $z5)
    ($x0 $y7 $z7)	//30
    ($x0 $y3 $z11)
    ($x0 $y3 $z12)
    ($x0 $y8 $z16)
    ($x0 $y4 $z15)
    ($x0 $y5 $z14)	//35
    ($x0 $y5 $z13)
    ($x0 $y5 $z12)
    ($x0 $y6 $z0)
    ($x0 $y6 $z3)
    ($x0 $y6 $z21)	//40
    ($x0 $y6 $z19)
    ($x0 $y6 $z18)
// filling blocks:
    ($x3 $y7 $z0)
    ($x3 $y7 $z3)
    ($x7 $y3 $z11)	//45
    ($x7 $y3 $z12)
    ($x5 $y7 $z0)
    ($x5 $y7 $z1)
    ($x7 $y5 $z13)
    ($x7 $y5 $z12)	//50
    ($x1 $y7 $z5)
    ($x2 $y1 $z10)
    ($x53 $y53 $z7)
    ($x5 $y7 $z2)
    ($x4 $y7 $z4)	//55
    ($x7 $y5 $z14)	
    ($x7 $y4 $z15)
    ($x2 $y7 $z5)
    ($x7 $y2 $z10)
    ($x8 $y7 $z6)	//60
    ($x7 $y8 $z16)
    ($x6 $y62 $z8) 
    ($x6 $y63 $z9)
    ($x64 $y6 $z19)
    ($x65 $y6 $z18)	//65
    ($x6 $y66 $z20)
    ($x67 $y6 $z21)
    ($x6 $y3 $z11)
    ($x3 $y6 $z3)
    ($x6 $y3 $z12)	//70
    ($x3 $y6 $z0)
    ($x6 $y5 $z12)
    ($x6 $y5 $z13)
    ($x5 $y6 $z0)
    ($x5 $y6 $z1)	//75
    ($x4 $y6 $z4)
    ($x5 $y6 $z2)
    ($x6 $y5 $z14)	
    ($x6 $y4 $z15)
    ($x86 $y6 $z21)	//80
    ($x6 $y81 $z20)
    ($x6 $y82 $z9)
    ($x83 $y6 $z19)
    ($x6 $y84_2 $z84_2)
//    ($x6 $y84 $z8)
    ($x85_2 $y85_2 $z85_2)	//85
    ($x86_2 $y6 $z86_2)    
);

blocks
(
// bulk blocks:
// block z0: blz0    
    hex (0 9 43 27 1 10 44 28) 		($nx_0 $ny_0 $nz_0) simpleGrading (1 1 $alphaM1)	//0
    hex (9 17 47 43 10 18 48 44) 	($nx_1 $ny_0 $nz_0) simpleGrading (1 1 $alphaM1)
    hex (27 43 71 38 28 44 69 39) 	($nx_0 $ny_2 $nz_0) simpleGrading (1 1 $alphaM1)
    hex (43 47 74 71 44 48 75 69) 	($nx_1 $ny_2 $nz_0) simpleGrading (1 1 $alphaM1)
// blz1
    hex (1 10 44 28 2 7 51 29) 		($nx_0 $ny_0 $nz_1) simpleGrading (1 1 1)	
    hex (28 44 69 39 29 51 67 40) 	($nx_0 $ny_2 $nz_1) simpleGrading (1 1 1)	// 5
// blz2
    hex (2 7 51 29 3 11 53 30) 		($nx_0 $ny_0 $nz_2) simpleGrading (1 1 1)
    hex (29 51 67 40 30 53 64 41) 	($nx_0 $ny_2 $nz_2) simpleGrading (1 1 1)  
// blz3
    hex (3 11 53 30 4 12 52 25 ) 	($nx_0 $ny_0 $nz_3) simpleGrading (1 1 1)
    hex (11 21 63 53 12 22 66 52) 	($nx_2 $ny_0 $nz_3) simpleGrading (1 1 1)
// blz4
    hex (4 12 52 25 5 13 45 31 ) 	($nx_0 $ny_0 $nz_4) simpleGrading (1 1 1)	//10
    hex (12 22 66 52 13 23 68 45) 	($nx_2 $ny_0 $nz_4) simpleGrading (1 1 1)
// blz5
    hex (5 13 45 31 6 14 46 32) 	($nx_0 $ny_0 $nz_5) simpleGrading (1 1 $alphaM2)		 
    hex (31 45 49 36 32 46 50 37) 	($nx_0 $ny_1 $nz_5) simpleGrading (1 1 $alphaM2)	
    hex (13 23 68 45 14 24 70 46) 	($nx_2 $ny_0 $nz_5) simpleGrading (1 1 $alphaM2)
    hex (45 68 73 49 46 70 72 50) 	($nx_2 $ny_1 $nz_5) simpleGrading (1 1 $alphaM2)	//15
    
// spacer blocks:
// x-oriented
    hex (10 18 48 44 15 19 54 55) 	($nx_1 $ny_0 $nbBlSpc) simpleGrading (1 1 $alphaY1)
    hex (7  10 44 51  8 15 55 58) 	($nz_1 $ny_0 $nbBlSpc) simpleGrading (1 1 $alphaY1)
    hex (11  7 51 53 16  8 58 60) 	($nz_2 $ny_0 $nbBlSpc) simpleGrading (1 1 $alphaY1)
    hex (21 11 53 63 20 16 60 62) 	($nx_2 $ny_0 $nbBlSpc) simpleGrading (1 1 $alphaY1)
//    hex (21 11 53 63 20 16 60 62) 	($nz_3 $ny_0 $nbBlSpc) simpleGrading (1 1 $alphaY1)
    hex (44 48 75 69 55 54 77 76) 	($nx_1 $ny_2 $nbBlSpc) simpleGrading (1 1 $alphaY1)	//20
    hex (51 44 69 67 58 55 76 80) 	($nz_1 $ny_2 $nbBlSpc) simpleGrading (1 1 $alphaY1)
    hex (53 51 67 64 60 58 80 83) 	($nz_2 $ny_2 $nbBlSpc) simpleGrading (1 1 $alphaY1)

// // y-oriented
    hex (34 57 56 35 31 45 49 36) 	($nx_0 $ny_1 $nbBlSpc) simpleGrading (1 1 $alphaX1)
    hex (26 59 57 34 25 52 45 31) 	($nx_0 $nz_4 $nbBlSpc) simpleGrading (1 1 $alphaX1)	
    hex (33 61 59 26 30 53 52 25) 	($nx_0 $nz_3 $nbBlSpc) simpleGrading (1 1 $alphaX1) //25
    hex (42 65 61 33 41 64 53 30) 	($nx_0 $nx_2 $nbBlSpc) simpleGrading (1 1 $alphaX1)
//    hex (42 65 61 33 41 64 53 30) 	($nx_0 $nz_2 $nbBlSpc) simpleGrading (1 1 $alphaX1)
    hex (57 79 78 56 45 68 73 49) 	($nx_2 $ny_1 $nbBlSpc) simpleGrading (1 1 $alphaX1)
    hex (59 81 79 57 52 66 68 45) 	($nx_2 $nz_4 $nbBlSpc) simpleGrading (1 1 $alphaX1)
    hex (61 82 81 59 53 63 66 52) 	($nx_2 $nz_3 $nbBlSpc) simpleGrading (1 1 $alphaX1)
// // common 
    hex (53 63 62 60 61 82 84 85) 	($nx_2 $nbBlSpc $nbBlSpc) simpleGrading (1 $alphaY1 $alphaY1) //30
    hex (64 53 60 83 65 61 85 86) 	($nx_2 $nbBlSpc $nbBlSpc) simpleGrading (1 $alphaY1 $alphaY1)  
 );

//arc support coordinates
phi0	#calc "0.5*$alpha";
phi1	#calc "0.5*$alpha+0.25*$pi";
phi2	#calc "5.0/8.0*$pi";

sx0 	#calc "$lx-$r0*sin($phi0)";
sz0 	#calc "$r0+$r0*cos($phi0)";

sx1 	#calc "$lx-$r1*sin($phi0)";
sz1 	#calc "$r0+$r1*cos($phi0)";

sy2	$y6;
sz2	$z12;
sy3	#calc "$ly-$r1*sin(3.0/8.0*$pi)";
sz3	#calc "$lz-$r0+$r1*cos(3.0/8.0*$pi)";

phiX6083	#calc "acos((0.5*($z6+$z19)-$r0)/$r0)";
sx6083		#calc "$lx-$r0*sin($phiX6083)";
phiY6083	#calc "acos((0.5*($z6+$z19)-$lz+$r0)/$r1)";
sy6083		#calc "$ly-$r0*sin($phiY6083)";

phiX8261	#calc "acos((0.5*($z9+$z16)-$r1)/$r0)";
phiY8261	#calc "acos((0.5*($z9+$z16)-$lz+$r1)/$r0)";
sx8261		#calc "$lx-$r0*sin($phiX8261)";	
sy8261		#calc "$ly-0.95*$r0*sin($phiY8261)";

//x65	#calc "$lx-$r0*sin($phiX65)";

// elpA	#calc "$y82-$y62";
// elpB	#calc "$z9-$z8";

// phiS	#calc "0.2*$pi/4.0";
// rPhi	#calc "$elpA*$elpB/sqrt( pow($elpB*cos($phiS),2) +pow($elpA*sin($phiS),2))";
// sy8284_1	#calc "$y62+$rPhi*cos($phiS)";
// sz8284_1	#calc "$z9-$rPhi*sin($phiS)";

// phiS	#calc "0.4*$pi/4.0";
// rPhi	#calc "$elpA*$elpB/sqrt( pow($elpB*cos($phiS),2) +pow($elpA*sin($phiS),2))";
// sy8284_2	#calc "$y62+$rPhi*cos($phiS)";
// sz8284_2	#calc "$z9-$rPhi*sin($phiS)";

// phiS	#calc "0.6*$pi/4.0";
// rPhi	#calc "$elpA*$elpB/sqrt( pow($elpB*cos($phiS),2) +pow($elpA*sin($phiS),2))";
// sy8284_3	#calc "$y62+$rPhi*cos($phiS)";
// sz8284_3	#calc "$z9-$rPhi*sin($phiS)";

// phiS	#calc "0.8*$pi/4.0";
// rPhi	#calc "$elpA*$elpB/sqrt( pow($elpB*cos($phiS),2) +pow($elpA*sin($phiS),2))";
// sy8284_4	#calc "$y62+$rPhi*cos($phiS)";
// sz8284_4	#calc "$z9-$rPhi*sin($phiS)";

// phiS	#calc "1.2*$pi/4.0";
// rPhi	#calc "$elpA*$elpB/sqrt( pow($elpB*cos($phiS),2) +pow($elpA*sin($phiS),2))";
// sy8462_1	#calc "$y62+$rPhi*cos($phiS)";
// sz8462_1	#calc "$z9-$rPhi*sin($phiS)";

// phiS	#calc "1.4*$pi/4.0";
// rPhi	#calc "$elpA*$elpB/sqrt( pow($elpB*cos($phiS),2) +pow($elpA*sin($phiS),2))";
// sy8462_2	#calc "$y62+$rPhi*cos($phiS)";
// sz8462_2	#calc "$z9-$rPhi*sin($phiS)";

// phiS	#calc "1.6*$pi/4.0";
// rPhi	#calc "$elpA*$elpB/sqrt( pow($elpB*cos($phiS),2) +pow($elpA*sin($phiS),2))";
// sy8462_3	#calc "$y62+$rPhi*cos($phiS)";
// sz8462_3	#calc "$z9-$rPhi*sin($phiS)";

// phiS	#calc "1.8*$pi/4.0";
// rPhi	#calc "$elpA*$elpB/sqrt( pow($elpB*cos($phiS),2) +pow($elpA*sin($phiS),2))";
// sy8462_4	#calc "$y62+$rPhi*cos($phiS)";
// sz8462_4	#calc "$z9-$rPhi*sin($phiS)";

// phiS	#calc "0.2*$pi/4.0";
// rPhi	#calc "$elpA*$elpB/sqrt( pow($elpB*cos($phiS),2) +pow($elpA*sin($phiS),2))";
// sy6185_1	#calc "$y7+$rPhi*cos($phiS)";
// sz6185_1	#calc "$z16-$rPhi*sin($phiS)";

// phiS	#calc "0.4*$pi/4.0";
// rPhi	#calc "$elpA*$elpB/sqrt( pow($elpB*cos($phiS),2) +pow($elpA*sin($phiS),2))";
// sy6185_2	#calc "$y7+$rPhi*cos($phiS)";
// sz6185_2	#calc "$z16-$rPhi*sin($phiS)";

// phiS	#calc "0.6*$pi/4.0";
// rPhi	#calc "$elpA*$elpB/sqrt( pow($elpB*cos($phiS),2) +pow($elpA*sin($phiS),2))";
// sy6185_3	#calc "$y7+$rPhi*cos($phiS)";
// sz6185_3	#calc "$z16-$rPhi*sin($phiS)";

// phiS	#calc "0.8*$pi/4.0";
// rPhi	#calc "$elpA*$elpB/sqrt( pow($elpB*cos($phiS),2) +pow($elpA*sin($phiS),2))";
// sy6185_4	#calc "$y7+$rPhi*cos($phiS)";
// sz6185_4	#calc "$z16-$rPhi*sin($phiS)";


edges
(
    arc 15 8 	($sx0 $y0 $sz0) 
    arc 10 7 	($sx1 $y0 $sz1)
 
    arc 55 58 	($sx0 $y7 $sz0) 
    arc 44 51 	($sx1 $y7 $sz1) 

    arc 76 80 	($sx0 $y6 $sz0) 
    arc 69 67 	($sx1 $y6 $sz1) 

    arc  8 16 	($sx0 $y0 $sz0) 
    arc 7 11 	($sx1 $y0 $sz1) 

    arc 58 60 	($sx0 $y7 $sz0) 
    arc 51 53 	($sx1 $y7 $sz1) 

    arc 64 65 	($sx1 $y6 $sz1) 

    arc 80 83 	($sx0 $y6 $sz0) 
    arc 67 64 	($sx1 $y6 $sz1) 

    arc 16 20 	($sx0 $y0 $sz0) 
    arc 11 21 	($sx1 $y0 $sz1) 

    arc 60 62 	($sx0 $y62 $sz0) 
    arc 53 63 	($sx1 $y63 $sz1) 

//.................
    arc 26 34  	($x0 $sy2 $sz2)
    arc 25 31  	($x0 $sy3 $sz3)
    
    arc 59 57  	($x7 $sy2 $sz2) 
    arc 52 45  	($x7 $sy3 $sz3) 

    arc 81 79  	($x6 $sy2 $sz2) 
    arc 68 66  	($x6 $sy3 $sz3) 

    arc 33 26  	($x0 $sy2 $sz2) 
    arc 30 25  	($x0 $sy3 $sz3)

    arc 61 59  	($x7 $sy2 $sz2) 
    arc 53 52  	($x7 $sy3 $sz3) 
    
    arc 42 33  	($x0 $sy2 $sz2) 
    arc 41 30  	($x0 $sy3 $sz3)

    arc 65 61  	($x7 $sy2 $sz2) 
    arc 64 53  	($x7 $sy3 $sz3) 

    arc 82 81  	($x6 $sy2 $sz2) 
    arc 63 66  	($x6 $sy3 $sz3) 

    arc 62 63  	($x6 $sy3 $sz3)  

     // arc 82 61 (
     // 	#calc "$x85_2+0.4*($x6-$x85_2)"
     // 	#calc "$y84_2+0.4*($y85_2-$y84_2)"
     // 	#calc "$z85_2+0.5*($z84_2-$z85_2)" 
     // 	)
    
    arc 84 85 (
	#calc "$x85_2+0.4*($x6-$x85_2)"
	#calc "$y84_2+0.4*($y85_2-$y84_2)"
	#calc "$z85_2+0.5*($z84_2-$z85_2)" 
	)

    arc 86 85 (
	#calc "$x86_2+0.4*($x85_2-$x86_2)"
	#calc "$y85_2+0.5*($y6-$y85_2)"
	#calc "$z86_2+0.3*($z85_2-$z86_2)" 
	)

    arc 60 83 ( $sx6083 $sy6083 #calc "0.5*($z6+$z19)")
    arc 82 61 ( $sx8261 $sy8261 #calc "0.5*($z9+$z16)")

);

boundary
(
    y0Sym
    {
    	type patch;
    	faces
    	(
    	    (0 9 10 1)
    	    (9 17 18 10)
    	    (10 18 19 15)
    	    (10 15 8 7)
    	    (1 10 7 2)
    	    (2 7 11 3)
    	    (7 8 16 11)
    	    (3 11 12 4)
    	    (11 21 22 12)
    	    (11 16 20 21)
    	    (4 12 13 5)
    	    (12 22 23 13)
    	    (5 13 14 6)
    	    (13 23 24 14)
    	);
    }
    x0Sym
    {
    	type patch;
    	faces
    	(
    	    (0 1 28 27)
    	    (27 28 39 38)
    	    (28 29 40 39)
    	    (29 30 41 40)
    	    (30 33 42 41) 
    	    (25 26 33 30)
    	    (31 34 26 25)
    	    (34 31 36 35)
    	    (32 37 36 31)
    	    (1 2 29 28)
    	    (2 3 30 29)
    	    (3 4 25 30)
    	    (4 5 31 25)
    	    (5 6 32 31)
    	);
    }
    Z0
    {
    	type wall;
    	faces
    	(
    	    (0 27 43 9)
    	    (17 9 43 47)
    	    (43 71 74 47)
    	    (71 43 27 38)
	    
    	);
    }
    Z1
    {
    	type wall;
    	faces
    	(
    	    (6 14 46 32)
    	    (37 32 46 50)
    	    (50 46 70 72)
    	    (70 46 14 24)
    	);
    }
    Y1
    {
    	type wall;
    	faces
    	(
    	    (38 39 69 71)
    	    (74 71 69 75)
    	    (75 69 76 77)
    	    (39 40 67 69)
    	    (76 69 67 80)
    	    (80 67 64 83)
    	    (83 64 65 86)
    	    (40 41 64 67)
    	    (41 42 65 64)
    	);
    }
    X1
    {
    	type patch;
    	faces
    	(
    	    (21 20 62 63)
    	    (62 84 82 63)
    	    (63 82 81 66)
    	    (66 81 79 68)
    	    (68 79 78 73)
    	    (68 73 72 70)
    	    (22 21 63 66)
    	    (23 22 66 68)
    	    (24 23 68 70)
    	);
    }
    spacer
    {
    	type wall;
    	faces
    	(
    	    (17 18 47 48)
    	    (19 18 48 54)
    	    (15 19 54 55)
    	    (8 15 55 58)
    	    (16 8 58 60)
    	    (20 16 60 62)
    	    (48 47 74 75)
    	    (54 48 75 77)
    	    (55 54 77 76)
    	    (58 55 76 80)
    	    (60 58 80 83)
    	    (85 60 83 86)
    	    (62 60 85 84)
    	    (61 85 86 65)
    	    (82 84 85 61)
    	    (82 61 59 81)
    	    (79 81 59 57)
    	    (79 57 56 78)
    	    (61 65 42 33)
    	    (59 61 33 26)
    	    (57 59 26 34)
    	    (56 57 34 35)
    	    (35 36 49 56)
    	    (50 49 36 37)
    	    (73 78 56 49)
    	    (72 73 49 50)
    	);
    }
);

mergePatchPairs
(
);

// ************************************************************************* //
